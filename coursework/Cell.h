#pragma once
#include "MapObject.h"

class Cell
{
public:
	enum Type { Passable, Blocked } type = Passable; // accessibility type
	unsigned short terrain = 0;
	
	MapObject* object = nullptr;

	bool isPassable() const
	{
		return (type != Type::Blocked) && (terrain == 0) && (object == nullptr);
	}
};
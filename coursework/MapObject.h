#pragma once
#include <SFML/Graphics.hpp>

class MapObject : public sf::Drawable
{
protected:
	sf::Vector2u position;
public:
	virtual void update(const sf::Time& deltaTime) = 0;
	virtual void loadFromReader(BinaryReader& reader) = 0;
	
	sf::Vector2u getMapPosition() const
	{
		return position;
	}
protected:
	void draw(sf::RenderTarget& target, sf::RenderStates states) const override = 0;
};
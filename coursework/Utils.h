#pragma once
#include <SFML/System/Vector2.hpp>

#include <algorithm>
#include <optional>
#include <cmath>
#include <list>

#include "Board.h"

inline std::optional<std::list<sf::Vector2u>> getPath(const sf::Vector2u start, const sf::Vector2u end, const Board& board)
{
	if (!board.isPassable(end))
		return std::nullopt;

	struct node
	{
		sf::Vector2u pos;
		double distFrom;
		double heuristic;
		node* prevNode;
	};

	auto dist = [](const sf::Vector2u & a, const sf::Vector2u & b)->double {
		return std::sqrt(std::pow(static_cast<double>(a.x) - b.x, 2) + std::pow(static_cast<double>(a.y) - b.y, 2));
	};

	std::list<node> open;
	std::list<node> closed;

	open.push_back({ start, 0, dist(start, end), nullptr });

	while (!open.empty())
	{
		auto current = std::min_element(std::begin(open), std::end(open),
			[](node & a, node & b) ->bool { return a.heuristic < b.heuristic; }
		);

		// if we connected start with end
		if (current->pos == end)
		{
			std::list<sf::Vector2u> path;
			node* it = &(*current);

			while (it != nullptr)
			{
				path.push_back(it->pos);
				it = it->prevNode;
			}
			return std::make_optional(path);
		}

		closed.push_back(*current);
		open.erase(current);

		std::list<sf::Vector2u> neighbors = board.getPassableNeighbors(closed.back().pos);

		for (auto& neighbor : neighbors)
		{
			auto isEqual = [&](node & b)->bool { return neighbor == b.pos; };

			if (std::find_if(closed.begin(), closed.end(), isEqual) == closed.end())
			{
				auto duplicate = std::find_if(open.begin(), open.end(), isEqual);
				auto currentDist = closed.back().distFrom + 1;
				currentDist += 0.414 * ((closed.back().pos.x != neighbor.x) && closed.back().pos.y != neighbor.y);

				if (duplicate == open.end())
				{
					open.push_back({ neighbor, currentDist, currentDist + dist(neighbor, end), &closed.back() });
				}
				else if (duplicate->distFrom > currentDist)
				{
					duplicate->distFrom = currentDist;
					duplicate->prevNode = &closed.back();
				}
			}
		}
	}

	return std::nullopt;
}
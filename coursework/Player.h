#pragma once
#include "Engine/Engine.hpp"
#include "MapObject.h"

class Player : public MapObject
{
private:
	AnimatedMultistateSprite sprite;
	Movable movable;

public:
	Player() : movable(sprite)
	{
		sprite.setOrigin(32, 32);
	}

	Player& operator=(Player& player)
	{
		this->sprite = std::move(player.sprite);
		this->movable = std::move(player.movable);
		return *this;
	}
	Player& operator=(Player&& player) noexcept
	{
		this->sprite = std::move(player.sprite);
		this->movable = std::move(player.movable);
		return *this;
	}
	void setState(int state)
	{
		sprite.setState(state);
	}
	void move(const sf::Vector2u& mapPosition, const sf::Vector2i& windowPosition)
	{
		auto turnPlayer = [&](bool r, int st) {
			this->right(r);
			sprite.setState(st);
		};

		movable.start(sf::Vector2f(windowPosition));

		auto diff = sf::Vector2i(mapPosition) - sf::Vector2i(position);
		if ((diff.x < 0) && (diff.y < 0))
		{
			turnPlayer(false, 8);
		}
		else if ((diff.x > 0) && (diff.y < 0))
		{
			turnPlayer(true, 8);
		}
		else if ((diff.x == 0) && (diff.y < 0))
		{
			turnPlayer(true, 9);
		}
		else if ((diff.x < 0) && (diff.y > 0))
		{
			turnPlayer(false, 6);
		}
		else if ((diff.x > 0) && (diff.y > 0))
		{
			turnPlayer(true, 6);
		}
		else if ((diff.x == 0) && (diff.y > 0))
		{
			turnPlayer(true, 5);
		}
		else if ((diff.x < 0) && (diff.y == 0))
		{
			turnPlayer(false, 7);
		
		}
		else if ((diff.x > 0) && (diff.y == 0))
		{
			turnPlayer(true, 7);
		}

		position = mapPosition;
	}
	bool gotToDest() const
	{
		return !movable.isMoving();
	}
	void stopMoving()
	{
		movable.stop();
		this->right(false);
		sprite.setState(0);
	}

	void update(const sf::Time& deltaTime) override
	{
		movable.update(deltaTime);
		sprite.update(deltaTime);
	}

	void loadFromReader(BinaryReader& reader) override
	{
		reader >> position.x >> position.y;
	}
	void loadSprite(const std::string& image, const std::string& config)
	{
		if (sprite.loadFromFile(image, config))
		{
			sprite.setState(0);
		}
		else
		{
			std::cerr << "Can't load animated image: " << image;
			std::cerr << " with config: " << config << std::endl;
		}
	}
	void setPosition(sf::Vector2f position)
	{
		sprite.setPosition(position);
	}
protected:
	virtual void draw(sf::RenderTarget& target, sf::RenderStates states) const override
	{
		target.draw(sprite);
	}

private:
	void right(bool right)
	{
		sf::Vector2f origin(32, 32);

		if (right)
		{
			sprite.setScale(1, 1);
			sprite.setOrigin(origin);
		}
		else
		{
			sprite.setScale(-1, 1);
			sprite.setOrigin(sprite.getLocalBounds().width - origin.x, origin.y);
		}
	}
};
#pragma once
#include <SFML/System/Vector2.hpp>

#include "Engine/BinaryFiles/BinaryReader.hpp"

#include "Cell.h"

class Board
{
private:
	sf::Vector2u size;

	Cell** board = nullptr;

public:
	~Board()
	{
		if (board != nullptr)
		{
			for (size_t i = 0; i < size.x; i++)
			{
				delete[] board[i];
			}
			delete[] board;
		}
	}

	Board& operator=(Board&& board) noexcept
	{
		this->size = std::move(board.size);
		this->board = std::move(board.board);
		
		// clearing data in board object for not deleting data on destruct
		board.board = nullptr;
		return *this;
	}

	bool isPassable(const sf::Vector2u& position) const
	{
		return ((position.x < size.x && position.y < size.y)
			&& board[position.x][position.y].isPassable());
	}

	Cell& operator[](const sf::Vector2u pos)
	{
		return board[pos.x][pos.y];
	}

	sf::Vector2u getSize() const
	{
		return size;
	}

	std::list<sf::Vector2u> getPassableNeighbors(const sf::Vector2u& current) const
	{
		std::list<sf::Vector2u> neighbors;

		bool main[4];

		sf::Vector2u temp = {current.x, current.y - 1};
		// ReSharper disable CppUsingResultOfAssignmentAsCondition
		if (main[0] = this->isPassable(temp))
		{
			neighbors.push_back(temp);
		}
		temp = { current.x, current.y + 1 };
		if (main[1] = this->isPassable(temp))
		{
			neighbors.push_back(temp);
		}
		temp = { current.x - 1, current.y };
		if (main[2] = this->isPassable(temp))
		{
			neighbors.push_back(temp);
		}
		temp = { current.x + 1, current.y };
		if (main[3] = this->isPassable(temp))
		{
			neighbors.push_back(temp);
		}
		// ReSharper restore CppUsingResultOfAssignmentAsCondition

		temp = { current.x - 1, current.y + 1 };
		if (main[1] && main[2] && this->isPassable(temp))
		{
			neighbors.push_back(temp);
		}
		temp = { current.x + 1, current.y + 1 };
		if (main[3] && main[1] && this->isPassable(temp))
		{
			neighbors.push_back(temp);
		}
		temp = { current.x + 1, current.y - 1 };
		if (main[0] && main[3] && this->isPassable(temp))
		{
			neighbors.push_back(temp);
		}
		temp = { current.x - 1, current.y - 1 };
		if (main[0] && main[2] && this->isPassable(temp))
		{
			neighbors.push_back(temp);
		}

		return neighbors;
	}

	bool loadFromReader(BinaryReader& reader) try
	{
		reader >> size.x >> size.y;
		this->allocate();

		for (size_t x = 0; x < size.x; x++)
		{
			for (size_t y = 0; y < size.y; y++)
			{
				reader >> board[x][y].terrain;
			}
		}

		return true;
	}
	catch (...)
	{
		return false;
	}
		
private:

	void allocate()
	{
		board = new Cell*[size.x];
		for (size_t i = 0; i < size.x; i++)
		{
			board[i] = new Cell[size.y];
		}
	}
};
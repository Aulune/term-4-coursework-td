#include <SFML/Graphics.hpp>

#include <iostream>

#include "Engine/Engine.hpp"

#include "TestScene.h"

int main() try
{
	sf::RenderWindow window(sf::VideoMode(920, 600), L"Path Finding", sf::Style::Default, sf::ContextSettings(0, 0, 8));

	Camera camera(window);	

	SceneManager::addScene("test", [&]()->Scene* { return new TestScene(window, camera); });
	SceneManager::loadScene("test");
	
	sf::Clock frameClock;
	while (window.isOpen())
	{
		Scene* const scene = SceneManager::getActiveScene();

		sf::Event event;
		while (window.pollEvent(event))
		{
			if (event.type == sf::Event::Closed)
				window.close();
			else if (event.type == sf::Event::Resized)
				camera.reset();
			else scene->input(event);
		}

		sf::Time frameTime = frameClock.restart();

		scene->update(frameTime);

		scene->gc();

		window.clear();
		scene->draw();
		window.display();
	}

	SceneManager::clear();
	
	return 0;
}
catch (std::exception& exception)
{
	std::cerr << "An error has occurred: ";
	std::cerr << exception.what() << std::endl;
	system("pause");
	return -1;
}
#pragma once
#include <vector>

#include "Engine/BinaryFiles/BinaryReader.hpp"

#include "Player.h"
#include "Board.h"

class MapLoader
{
private:
	std::pair <char const * const, char const* const> playersData[1] =
	{
		{ "./Data/Images/hero.png", "./Data/Images/hero.acnf" }
	};

	// map data
	Board board;
	std::vector<Player*> players;

public:
	MapLoader() = default;

	bool loadMap(const std::string& filename)
	{
		BinaryReader reader;
		reader.open(filename);

		// loading board
		board.loadFromReader(reader);

		// loading players
		players.resize(reader.read<size_t>());
		
		for (auto& player : players)
		{
			player = new Player();

			auto& spriteData = playersData[reader.read<unsigned short>()];
			player->loadSprite(spriteData.first, spriteData.second);
			player->loadFromReader(reader);

			auto pos = player->getMapPosition();
			board[pos].object = player;
			player->setPosition(sf::Vector2f(pos.x * 32, pos.y * 32));
		}
		
		reader.close();
		return true;
	}

	Board& getBoard()
	{
		return board;
	}
	std::vector<Player*>& getPlayers()
	{
		return players;
	}
};
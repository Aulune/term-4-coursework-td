#pragma once
#include "Engine/Engine.hpp"

#include <optional>

#include "MapLoader.h"
#include "Board.h"
#include "Utils.h"

class TestScene : public Scene
{
private:
	Board board;

	sf::Texture texture;
	std::vector<sf::RectangleShape> cell;

	sf::Vector2u start, end;
	std::list<sf::Vector2u> path;
	std::list<sf::Vector2u>::reverse_iterator iterator = path.rend();
	std::vector<Player*> players;

	bool moving = false;

public:
	TestScene(sf::RenderWindow& window, Camera& camera)
		: Scene(window, camera)
	{
		MapLoader loader;
		loader.loadMap("./Data/Test/map.map");
		
		board = std::move(loader.getBoard());
		players = std::move(loader.getPlayers());

		texture.loadFromFile("./Data/Test/test.png");

		sf::RectangleShape rect;
		rect.setSize({ 32.f, 32.f });
		rect.setTexture(&texture);

		rect.setTextureRect({ 0, 0, 64, 64 });
		cell.push_back(rect);

		rect.setTextureRect({ 64, 64, 64, 64 });
		cell.push_back(rect);

		start = end = { 0, 0 };
	}
	
	void update(const sf::Time& deltaTime) override
	{
		if (moving && players[0]->gotToDest())
		{
			if (iterator == path.rend())
			{
				players[0]->stopMoving();
				moving = false;
			}
			else
			{
				//path.erase(std::next(iterator).base());
				++iterator;
				if (iterator != path.rend())
				{
					const int side = cell[0].getSize().x;
					const auto pos = window.mapCoordsToPixel(sf::Vector2f((*iterator).x * side, (*iterator).y * side));

					board[players[0]->getMapPosition()].object = nullptr;
					players[0]->move(*iterator, pos);
					board[players[0]->getMapPosition()].object = players[0];
				}
			}
		}

		for (auto& player : players)
			player->update(deltaTime);
	}
	
	void input(const sf::Event & event) override
	{
		if (event.type == sf::Event::MouseButtonPressed)
		{
			if ((event.mouseButton.button == sf::Mouse::Left) && !moving)
			{				
				const auto side = cell[0].getSize().x;

				const auto pos = window.mapPixelToCoords(sf::Vector2i(event.mouseButton.x, event.mouseButton.y));
				end = sf::Vector2u(pos.x / side, pos.y / side);

				if (players[0]->getMapPosition() != end)
				{
					auto opt = getPath(players[0]->getMapPosition(), end, board);
					if (opt.has_value())
					{
						path.clear();
						path = opt.value();
						iterator = path.rbegin();
						moving = true;
					}
				}
			}
		}
		else if (event.type == sf::Event::KeyPressed)
		{
			const auto side = cell[0].getSize().x / 2;
			switch (event.key.code)
			{
			case sf::Keyboard::Up:
				camera.move({ 0, -side });
				break;
			case sf::Keyboard::Down:
				camera.move({ 0, side });
				break;
			case sf::Keyboard::Left:
				camera.move({ -side, 0 });
				break;
			case sf::Keyboard::Right:
				camera.move({ side, 0 });
				break;
			case sf::Keyboard::Add:
				camera.zoom(0.5);
				break;
			case sf::Keyboard::Subtract:
				camera.zoom(1.5);
				break;
			default:
				break;
			}
		}
	}

	void gc() override
	{

	}

	void draw() override
	{
		const auto cellSize = cell[0].getSize().x;

		const auto size = board.getSize();
		for (size_t x = 0; x < size.x; x++)
		{
			for (size_t y = 0; y < size.y; y++)
			{
				const auto terrain = board[{x, y}].terrain;
				cell[terrain].setPosition(x * cellSize, y * cellSize);
				window.draw(cell[terrain]);
			}
		}

		if (!path.empty())
		{
			std::vector<sf::Vertex> line;
			for (auto& point : path)
			{
				sf::Vector2f position = { (point.x + 0.5f) * cellSize, (point.y + 0.5f) * cellSize };
				line.emplace_back(position, sf::Color::Red);
			}
			window.draw(&line.front(), line.size(), sf::LineStrip);
		}

		for (auto& player : players)
			window.draw(*player);
	}
};
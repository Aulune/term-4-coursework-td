#pragma once
#include <future>

template<typename T, typename... Args>
class AsyncFunction
{
private:
	std::future<T> thread;
	std::function<T(Args...)> func;

public:
	AsyncFunction(std::function<T(Args...)> func)
		: func(func)
	{ }

	void start(Args... args)
	{
		if (!this->isReady()) return;

		thread = std::async(std::launch::async, func, args...);
	}

	bool isReady() const
	{
		return (!thread.valid()) || (thread.wait_for(std::chrono::microseconds(0)) == std::future_status::ready);
	}

	void wait()
	{
		thread.wait();
	}

	T get()
	{
		return thread.get();
	}
};
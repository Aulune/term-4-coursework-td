#pragma once
#include <any>
#include <map>

class SharedData
{
private:
	static std::map<std::string, std::any> data;

public:
	static void add(const std::string& key, std::any value);

	// throws std::out_of_range / std::bad_any_cast
	template<typename T>
	static T get(const std::string& key);
	static std::any get(const std::string& key);

	static void remove(const std::string& key);

	static void clear();
};

std::map<std::string, std::any> SharedData::data;

inline void SharedData::add(const std::string& key, std::any value)
{
	data.insert({ key, value });
}

template<typename T>
T SharedData::get(const std::string& key)
{
	return std::any_cast<T>(data.at(key));
}

inline std::any SharedData::get(const std::string& key)
{
	return data.at(key);
}

inline void SharedData::remove(const std::string& key)
{
	data.erase(key);
}

inline void SharedData::clear()
{
	data.clear();
}

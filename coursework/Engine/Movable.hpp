#pragma once
#include <SFML/Graphics/Transformable.hpp>
#include <SFML/System/Time.hpp>

#include <cmath>

class Movable
{
protected:
	sf::Transformable& object;
	sf::Vector2f finishPoint;
	sf::Vector2f startPoint;

	bool moving = false;

public:
	Movable(sf::Transformable& object)
		:object(object)
	{}

	void operator=(Movable&& movable) noexcept
	{
		object = std::move(movable.object);
		finishPoint = movable.finishPoint;
		startPoint = movable.startPoint;
		moving = movable.moving;
	}

	void start(const sf::Vector2f& destination)
	{
		this->finishPoint = destination;
		this->startPoint = object.getPosition();
		moving = true;
	}
	void stop()
	{
		moving = false;
	}
	bool isMoving() const
	{
		return moving;
	}

	void update(const sf::Time& deltaTime)
	{
		if (!moving) return;

		auto pos = object.getPosition();

		float dist = std::sqrtf(std::pow(finishPoint.x - pos.x, 2) + std::pow(finishPoint.y - pos.y, 2));

		if (dist < 0.1)
		{
			moving = false;
			return;
		}

		sf::Vector2f offset;
		offset.x = (finishPoint.x - startPoint.x) * deltaTime.asSeconds();
		offset.y = (finishPoint.y - startPoint.y) * deltaTime.asSeconds();
		object.move(offset);
	}
};
#pragma once
class Disposable
{
private:
	bool toDispose = true;

public:
	void dispose()
	{
		toDispose = false;
	}
	bool isDisposed() const
	{
		return !toDispose;
	}
};
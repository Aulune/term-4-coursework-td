#pragma once
#include <map>
#include <functional>

#include "Scene.hpp"

class SceneManager
{
private:
	static std::map<std::string, std::function<Scene*()>> scenes;
	static Scene* activeScene;
	static std::string nextSceneName;

public:
	static void addScene(const std::string& name, const std::function<Scene*()>& scene);
	static void loadScene(const std::string& name);
	static void clear();

	static Scene* getActiveScene();
};
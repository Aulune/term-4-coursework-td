#pragma once
#include <SFML/Graphics/RenderWindow.hpp>

#include "../Disposable.hpp"
#include "../Camera.hpp"

class Scene : public Disposable
{
protected:
	sf::RenderWindow& window;
	Camera& camera;
public:
	Scene(sf::RenderWindow& window, Camera& camera)
		: window(window), camera(camera)
	{};

	virtual ~Scene() = default;

	virtual void update(const sf::Time& deltaTime) = 0;
	virtual void input(const sf::Event& event) = 0;
	virtual void gc() = 0; // for garbage collection
	virtual void draw() = 0;
};
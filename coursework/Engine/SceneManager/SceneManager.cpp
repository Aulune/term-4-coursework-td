#include "SceneManager.hpp"

std::map<std::string, std::function<Scene*()>> SceneManager::scenes;
Scene* SceneManager::activeScene = nullptr;
std::string SceneManager::nextSceneName = "";

void SceneManager::addScene(const std::string& name, const std::function<Scene*()>& scene)
{
	scenes.insert({ name, scene });
}

void SceneManager::loadScene(const std::string& name)
{
	nextSceneName = name;
	if (activeScene) activeScene->dispose();
	else activeScene = scenes.at(nextSceneName)();
}

Scene* SceneManager::getActiveScene()
{
	if (activeScene && activeScene->isDisposed())
	{
		delete activeScene;
		activeScene = scenes.at(nextSceneName)();
	}
	return activeScene;
}

void SceneManager::clear()
{
	delete activeScene;
	scenes.clear();
}
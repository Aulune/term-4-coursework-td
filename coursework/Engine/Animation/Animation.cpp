#include "Animation.hpp"

Animation::Animation(sf::Sprite &sprite) 
	: sprite(sprite)
{}

void Animation::addFrame(const sf::IntRect& frame)
{
	frames.push_back(std::move(frame));
}
void Animation::setCurrentFrame(const size_t frame)
{
	currentFrame = frame;
	this->setCurrentFrame();
}
void Animation::setCurrentFrame()
{
	sprite.setTextureRect(frames[currentFrame]);
}

void Animation::update(const sf::Time& elapsed)
{
	if (!playing) return;

	timer += elapsed;

	if (timer >= frameTime)
	{
		// keeping: timer - frameTime
		timer = sf::microseconds(timer.asMicroseconds() % frameTime.asMicroseconds());
		
		currentFrame++;

		if (currentFrame >= frames.size())
		{
			if (!looped)
			{
				playing = false;
				return;
			}
			currentFrame = 0;
		}

		this->setCurrentFrame();
	}
}

void Animation::play(bool reset)
{
	if (reset) this->reset();
	playing = true;
}
void Animation::stop()
{
	playing = false;
}
void Animation::reset()
{
	currentFrame = 0;
	timer = sf::seconds(0);
}

void Animation::setLooped(bool looped)
{
	this->looped = looped;
}
void Animation::setFrameTime(const sf::Time& frameTime)
{
	this->frameTime = frameTime;
}

bool Animation::isPlaying() const
{
	return playing;
}
bool Animation::isLooped() const
{
	return looped;
}

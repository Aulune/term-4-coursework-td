#include "AnimatedMultistateSprite.hpp"

#define Multistate AnimatedMultistateSprite

Multistate::Multistate() 
	: Sprite(texture)
{}

Multistate::Multistate(const std::string& filename)
	: Sprite(texture)
{
	this->Multistate::loadFromFile(filename);
}

Multistate::Multistate(const std::string& filename, const std::string& configFile)
	: Sprite(texture)
{
	this->Multistate::loadFromFile(filename, configFile);
}

Multistate& Multistate::operator=(const Multistate& animation)
{
	this->currentAnimation = animation.currentAnimation;
	this->animations	   = animation.animations;
	this->texture		   = animation.texture;
	this->state			   = animation.state;

	return *this;
}

Multistate& Multistate::operator=(Multistate&& animation) noexcept
{
	this->currentAnimation = std::move(animation.currentAnimation);
	this->animations	   = std::move(animation.animations);
	this->texture		   = std::move(animation.texture);
	this->state			   = std::move(animation.state);

	animation.animations.clear();

	return *this;
}

Multistate::~Multistate()
{
	for (auto& pair : animations)
		delete pair.second;
}

void Multistate::update(const sf::Time& deltaTime)
{
	currentAnimation->update(deltaTime);
}

void Multistate::addState(int index)
{
	animations[index] = new Animation(*this);
}
void Multistate::addFrame(int state, const sf::IntRect& frame)
{
	animations.at(state)->addFrame(std::move(frame));
}

void Multistate::setState(int state)
{
	// if requested animation is already playing
	if (this->state == state)
		return;

	// changing current animation & state
	currentAnimation = animations.at(state);
	this->state = state;
	// updating current frame
	currentAnimation->setCurrentFrame();
}
int Multistate::getState() const
{
	return state;
}

void Multistate::setFrameTime(const sf::Time& frameTime)
{
	// setting frame time for each state
	for (auto& pair : animations)
	{
		pair.second->setFrameTime(frameTime);
	}
}

void Multistate::createMaskFromColor(sf::Color color)
{
	sf::Image image = texture.copyToImage();
	image.createMaskFromColor(color);
	texture.loadFromImage(image);
}

bool Multistate::loadFromFile(const std::string& filename)
{
	return texture.loadFromFile(filename);
}
bool Multistate::loadConfigFile(const std::string& configFile)
{
	BinaryReader reader(configFile);

	if (!reader.is_open())
		return false;

	unsigned __int8 states, frames;

	// reading states number
	reader >> states;

	// reading frame time
	auto frameTime = sf::milliseconds(reader.read<unsigned int>());
	
	// reading states
	for (unsigned __int8 s = 0; s < states; s++)
	{
		// inserting new state
		this->addState(s);
		
		reader >> frames; // number of frames in current state 
		
		// link for current state
		auto& state = animations.at(s);
		state->setFrameTime(frameTime);
		
		// reading frames
		for (unsigned __int8 f = 0; f < frames; f++)
		{
			sf::IntRect rect;
			reader >> rect.left;
			reader >> rect.top;
			reader >> rect.width;
			reader >> rect.height;

			state->addFrame(std::move(rect));
		}
	}

	return true;
}
bool Multistate::loadFromFile(const std::string& filename, const std::string& configFile)
{
	return this->loadFromFile(filename) && loadConfigFile(configFile);
}

#undef Multistate
#include "AnimatedSprite.hpp"

AnimatedSprite::AnimatedSprite()
	: Sprite(texture), animation(*this)
{}

AnimatedSprite::AnimatedSprite(const std::string& filename)
	: Sprite(texture), animation(*this)
{
	this->AnimatedSprite::loadFromFile(filename);
}
AnimatedSprite::AnimatedSprite(const std::string& filename, const std::string& configFilename)
	: Sprite(texture), animation(*this)
{
	this->AnimatedSprite::loadFromFile(filename, configFilename);
}

void AnimatedSprite::update(const sf::Time& deltaTime)
{
	animation.update(deltaTime);
}

void AnimatedSprite::addFrame(const sf::IntRect& frame)
{
	animation.addFrame(std::move(frame));
}

void AnimatedSprite::createMaskFromColor(sf::Color color)
{
	sf::Image image = texture.copyToImage();
	image.createMaskFromColor(color);
	texture.loadFromImage(image);
}

bool AnimatedSprite::loadFromFile(const std::string& filename)
{
	return texture.loadFromFile(filename);
}
bool AnimatedSprite::loadConfigFile(const std::string& configFilename)
{
	BinaryReader reader(configFilename);

	if (!reader.is_open())
		return false;
	
	// skipping states byte
	reader.read<__int8>();

	// frame time
	animation.setFrameTime(sf::milliseconds(reader.read<unsigned int>()));
	
	// frames number
	unsigned __int8 frames = reader.read<unsigned __int8>();
	// reading frames
	for (unsigned __int8 f = 0; f < frames; f++)
	{
		sf::IntRect rect;
		reader >> rect.left;
		reader >> rect.top;
		reader >> rect.width;
		reader >> rect.height;

		this->addFrame(rect);
	}
	
	reader.close();
	return true;
}
bool AnimatedSprite::loadFromFile(const std::string& filename, const std::string& configFilename)
{
	return this->loadFromFile(filename) && loadConfigFile(configFilename);
}
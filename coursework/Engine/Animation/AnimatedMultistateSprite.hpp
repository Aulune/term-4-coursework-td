#pragma once
#include <SFML/Graphics/Texture.hpp>
#include <SFML/Graphics/Sprite.hpp>

#include <map>

#include "../BinaryFiles/BinaryReader.hpp"
#include "Animation.hpp"

// ReSharper disable once CppInconsistentNaming
#define Multistate AnimatedMultistateSprite

class Multistate : public sf::Sprite
{
private:
	sf::Texture texture;

	std::map<int, Animation*> animations;
	
	int state = -1;
	Animation* currentAnimation = nullptr;

public:
	Multistate();
	explicit Multistate(const std::string& filename);
	Multistate(const std::string& filename, const std::string& configFile);
	
	// copying operator
	Multistate& operator=(const Multistate& animation);
	// moving operator
	Multistate& operator=(Multistate&& animation) noexcept;

	virtual ~Multistate();

	virtual void update(const sf::Time& deltaTime);

	void addState(int index);
	void addFrame(int state, const sf::IntRect& frame);

	void setState(int state);
	int getState() const;

	void setFrameTime(const sf::Time& frameTime);

	void createMaskFromColor(sf::Color color);

	virtual bool loadFromFile(const std::string& filename);
	virtual bool loadConfigFile(const std::string& configFile);
	virtual bool loadFromFile(const std::string& filename, const std::string& configFile);
};

#undef Multistate
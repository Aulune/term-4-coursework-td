#pragma once
#include <SFML/Graphics/Sprite.hpp>
#include <SFML/Graphics/Texture.hpp>

#include "../BinaryFiles/BinaryReader.hpp"
#include "Animation.hpp"

class AnimatedSprite : public sf::Sprite
{
private:
	sf::Texture texture;
	
	Animation animation;

public:
	AnimatedSprite();
	explicit AnimatedSprite(const std::string& filename);
	AnimatedSprite(const std::string& filename, const std::string& configFilename);

	AnimatedSprite& operator=(const AnimatedSprite& animation) = default;	
	AnimatedSprite& operator=(AnimatedSprite&& animation) = default;

	void update(const sf::Time& deltaTime);

	void addFrame(const sf::IntRect& frame);

	void createMaskFromColor(sf::Color color);

	virtual bool loadFromFile(const std::string& filename);
	virtual bool loadConfigFile(const std::string& configFilename);
	virtual bool loadFromFile(const std::string& filename, const std::string& configFilename);
};
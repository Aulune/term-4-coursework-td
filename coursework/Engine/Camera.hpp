#pragma once
#include <SFML/Graphics/RenderWindow.hpp>

class Camera
{
private:
	sf::RenderWindow& window;
	sf::View view;
public:

	Camera(sf::RenderWindow& window, const sf::Vector2f& center)
		:window(window)
	{
		view.setCenter(center);
		this->reset();
	}
	Camera(sf::RenderWindow& window)
		:window(window)
	{
		view.setCenter({ window.getSize().x / 2.f, window.getSize().y / 2.f });
		this->reset();
	}

	void reset()
	{
		auto center = view.getCenter();

		view.reset(sf::FloatRect({ 0.f, 0.f }, sf::Vector2f(window.getSize())));
		
		this->setCenter(center);
	}
	void reset(const sf::Vector2f& position)
	{
		view.reset(sf::FloatRect({ 0.f, 0.f }, sf::Vector2f(window.getSize())));
		this->setCenter(position);
	}

	void move(const sf::Vector2f& offset)
	{
		view.move(offset);
		this->setView();
	}

	void zoom(float factor)
	{
		view.zoom(factor);
		this->setView();
	}

	void setCenter(const sf::Vector2f& position)
	{
		view.setCenter(position);
		this->setView();
	}

	sf::FloatRect getViewport() const
	{	
		return sf::FloatRect(window.mapPixelToCoords({ 0, 0 }), view.getSize());
	}

private:
	void setView()
	{
		window.setView(view);
	}
};
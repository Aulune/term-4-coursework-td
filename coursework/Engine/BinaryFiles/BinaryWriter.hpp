#pragma once
#include <fstream>

class BinaryWriter final
{
private:
	std::ofstream stream;

public:
	BinaryWriter() = default;

	explicit BinaryWriter(const std::string& filename) 
	{
		this->open(filename);
	}

	~BinaryWriter()
	{
		this->close();
	}
	
	template<typename T>
	void write(const T& variable)
	{
		stream.write(reinterpret_cast<const char*>(&variable), sizeof(T));
	}

	template<typename T>
	BinaryWriter& operator<<(const T& variable)
	{
		stream.write(reinterpret_cast<const char*>(&variable), sizeof(T));
		return *this;
	}

	void open(const std::string& filename)
	{
		stream.open(filename, std::ios::out | std::ios::binary);
	}
	void close()
	{
		stream.close();
	}

	bool is_open() const
	{
		return stream.is_open();
	}
};
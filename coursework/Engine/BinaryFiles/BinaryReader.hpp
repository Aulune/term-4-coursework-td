#pragma once
#include <fstream>

class BinaryReader final
{
private:
	typedef __int8 byte;

	std::ifstream stream;

public:
	BinaryReader() = default;

	explicit BinaryReader(const std::string& filename)
	{
		this->open(filename);
	}

	~BinaryReader()
	{
		this->close();
	}
	
	template<typename T>
	T read()
	{
		T variable;
		stream.read(reinterpret_cast<char*>(&variable), sizeof(T));
		return variable;
	}

	template<typename T>
	BinaryReader& operator>>(T& variable)
	{
		stream.read(reinterpret_cast<char*>(&variable), sizeof(T));
		return *this;
	}

	void open(const std::string& filename)
	{
		stream.open(filename, std::ios::in | std::ios::binary);
	}
	void close()
	{
		stream.close();
	}
	
	bool is_open() const
	{
		return stream.is_open();
	}

	explicit operator bool() const
	{
		return static_cast<bool>(stream);
	}
};
